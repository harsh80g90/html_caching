import os
from bs4 import BeautifulSoup
import urllib.request
import sys
from db import get_redis

#caching_html is to cache html pages and url as key
def caching_html(key, value):

    r = get_redis()

    try:
        value = r.set(key, value)
    except Exception as e:
        sys.exit("error in execution..", e)

    return

#parseXML is to get url from XML structure
def parseXML(xmlfile):
        
    fd = open(xmlfile, 'r')
    
    xml_file = fd.read()
    soup = BeautifulSoup(xml_file, 'lxml')
    
    for tag in soup.findAll("loc"):
        key_url = tag.text
        
        try:
            fp = urllib.request.urlopen(key_url)
        except Exception as e:
            print("Invalid url or unable to request:", e)
            
        mybytes = fp.read()
        html_value = mybytes.decode("utf8")
        
        print("Length of key:",len(key_url),
              " - Length of value:",len(html_value))
        
        caching_html(key_url, html_value)
        
        fp.close()
    fd.close()
    
def main():
    
    file_name = os.getenv("filePath")
    parseXML(file_name)
    return

if __name__ == "__main__":
    main()

