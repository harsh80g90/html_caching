import redis
import sys

# redis database connection
def get_redis():

    try:
        redis_conn = redis.StrictRedis('localhost',
                                        6379,
                                        decode_responses=True)
    except Exception as e:
        sys.exit("error in connection", e)

    return redis_conn

# get values from redis database
def get_value(key):

    r = get_redis()

    key = str(key)
    try:
        value = r.get(key)
    except Exception as e:
        sys.exit("error in execution..", e)

    return value
