from flask import Flask, request, jsonify
import os
from db import get_value

app = Flask(__name__)

def main(key):
    
    value = get_value(key)
    return value

@app.route('/url', methods=['POST', 'GET'])
def jobs():

    key = request.form.get("url")

    try:
        value = main(key)
    except Exception as e:
        print("error occured", e)
        value = {}

    result_dict = {"HTML_page": value}

    return jsonify(result_dict)

if __name__ == "__main__":
    app.run(host=os.getenv("PDIP"), port=5090)